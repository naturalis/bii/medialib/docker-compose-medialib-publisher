docker-compose-medialib-publisher
 
=====================
docker-compose definition for deployment of publisher / harvester for medialibrary


Docker-compose
--------------

contains : 
 - Application containers based on php-cli 
 - NFS client container
 - Container for Cronjobs
 - Traefik

It is started using Foreman or ansible which installs docker and docker-compose and  creates:
 - .env file


Result
------

Working publisher server

Limitations
-----------
This has been tested.
